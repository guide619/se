const express = require('express')
const app =express()
const morgan = require('morgan')
const mysql = require('mysql')
const path = require('path')

app.use(morgan('combined'))
app.use(express.static('../build'))

app.get('/course/:subject', (req, res)=>{
  console.log("Fetching user with id: " + req.params.subject )
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  
  //const course = req.params.id
  const queryString = "SELECT * FROM course WHERE subject LIKE '" + req.params.subject + "%'" 
  connection.query(queryString,(err,rows,fields)=>{
    if(err){
      console.log("Failed to query for users" + err)
      res.sendStatus(500)
      return
      //throw err
    }
    console.log("I think we fetched users successfully")

    const users = rows.map((row) => {
      return {courseID: row.courseID ,subject: row.subject, detail: row.detail,day: row.day ,time: row.time}
    })
    res.json(users)
  })
})

// app.get('/course/days/:day', (req, res)=>{
//   console.log("Fetching user with id: " + req.params.day )
//   const connection = mysql.createConnection({
//     host: 'localhost',
//     port: '3306',
//     user: 'root',
//     password: 'password',
//     database: 'sorn_mysql'
//   })
  
//   //const course = req.params.id
//   const queryString = "SELECT * FROM course WHERE day = '" + req.params.day + "'"
//   connection.query(queryString,(err,rows,fields)=>{
//     if(err){
//       console.log("Failed to query for users" + err)
//       res.sendStatus(500)
//       return
//       //throw err
//     }
//     console.log("I think we fetched users successfully")

//     const users = rows.map((row) => {
//       return {courseID: row.courseID ,subject: row.subject, detail: row.detail,day: row.day ,time: row.time}
//     })
//     res.json(users)
//   })
// })


// app.get('/course/time/:time', (req, res)=>{
//   console.log("Fetching user with id: " + req.params.time )
//   const connection = mysql.createConnection({
//     host: 'localhost',
//     port: '3306',
//     user: 'root',
//     password: 'password',
//     database: 'sorn_mysql'
//   })
  
//   //const course = req.params.id
//   const queryString = "SELECT * FROM course WHERE time = '" + req.params.time + "'" 
//   connection.query(queryString,(err,rows,fields)=>{
//     if(err){
//       console.log("Failed to query for users" + err)
//       res.sendStatus(500)
//       return
//       //throw err
//     }
//     console.log("I think we fetched users successfully")

//     const users = rows.map((row) => {
//       return {courseID: row.courseID ,subject: row.subject, detail: row.detail,day: row.day ,time: row.time}
//     })
//     res.json(users)
//   })
// })


app.get('/course/:subject/filter/:day', (req, res)=>{
  console.log("Fetching user with id: " + req.params.subject +"and"+req.params.day)
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  
  //const course = req.params.id
  const queryString = "SELECT * FROM course WHERE subject LIKE'" + req.params.subject+"%' and day ="+ "'" +req.params.day+"'" 
  connection.query(queryString,(err,rows,fields)=>{
    if(err){
      console.log("Failed to query for users" + err)
      res.sendStatus(500)
      return
      //throw err
    }
    console.log("I think we fetched users successfully")

    const users = rows.map((row) => {
      return {courseID: row.courseID ,subject: row.subject, detail: row.detail,day: row.day ,time: row.time}
    })
    res.json(users)
  })
})

app.get('/course', (req, res)=>{
  console.log("Fetching user with id: " + req.params.id)
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  
  const userId = req.params.id
  const queryString = "SELECT * FROM course"
  connection.query(queryString,[userId],(err,rows,fields)=>{
    if(err){
      console.log("Failed to query for users" + err)
      res.sendStatus(500)
      return
      //throw err
    }
    console.log("I think we fetched users successfully")

    const users = rows.map((row) => {
      return {courseID: row.courseID ,subject: row.subject, detail: row.detail,day: row.day ,time: row.time}
    })
    res.json(users)
  })
})



app.get("/", (req,res ) => {
  console.log("Responding to root route")
  res.sendFile(path.join(__dirname, '../build', 'index.html'))
})




app.listen(3000,() => {
  console.log("Server is up and listening on 3000...")
})
