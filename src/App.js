import React, { Component } from 'react';
import './App.css';
import CourseRow from './CourseRow.js'
import $ from 'jquery'
class App extends Component {
  constructor(props){
    super(props)
    this.state={}
    var data1 =""
    var data2 =""
    var data3 = ""
    //console.log("This is my initializer")
    
    // const courses= []

    // var courseRows = []
    // courses.forEach((course)=>{
    //   console.log(course.first_name)
    //   const CourseRow = <courseRow course={course} />
    //   courseRows.push(CourseRow)
    // })
    // this.state = {row: courseRows}
    this.performSearch(data1,data2,data3)
  }
  performSearch(searchTerm1,searchTerm2,searchTerm3) {
    console.log("Perform search using api")
    console.log(searchTerm1)
    const urlString = "http://localhost:3000/course/"+searchTerm1
    $.ajax({
      method: 'GET',
      url: urlString,
      success: (searchResults) =>{
        console.log("Fetched data successfully" + urlString)
        console.log(searchResults)
        var arr1=JSON.stringify(searchResults);
        var arr2=JSON.parse(arr1);
        //const results = searchResults.results
        console.log(arr2[0])
        var courseRows =[]
        //console.log(courseRows)
        arr2.forEach((course) => {
          //console.log(course.first_name)
          const courseRow = <CourseRow key={course.courseID} course ={course}/>
          courseRows.push(courseRow)
        })
   
        this.setState({rows: courseRows})
      },
      error: (xhr , status , err) => {
        console.error("Failed to fetch data")
      }
    })
  }


  performFilter(searchTerm1,searchTerm2,searchTerm3) {
    console.log("Perform search using api")
    console.log(searchTerm2)
    console.log(searchTerm1)
    const urlString = "http://localhost:3000/course/" +searchTerm1 + "/filter/"+searchTerm2
    $.ajax({
      method: 'GET',
      url: urlString,
      success: (searchResults) =>{
        console.log("Fetched data successfully" + urlString)
        console.log(searchResults)
        var arr1=JSON.stringify(searchResults);
        var arr2=JSON.parse(arr1);
        //const results = searchResults.results
        console.log(arr2[0])
        var courseRows =[]
        //console.log(courseRows)
        arr2.forEach((course) => {
          //console.log(course.first_name)
          const courseRow = <CourseRow key={course.courseID} course ={course}/>
          courseRows.push(courseRow)
        })
   
        this.setState({rows: courseRows})
      },
      error: (xhr , status , err) => {
        console.error("Failed to fetch data")
      }
    })
  }

  searchChangeHandler(event){
    console.log(event.target.value)
    const boundObject = this
    const searchTerm = event.target.value
    this.data1 = searchTerm.toString()
    boundObject.performSearch(this.data1)
  }
  selectDayChangeHandler(event){
    console.log(event.target.value)
    const boundObject = this
    const selectTerm = event.target.value
    this.data2 = selectTerm
    boundObject.performFilter(this.data1,this.data2,this.data3)
  }
  
  render() {
    return (
      <div className="App">
      <table align = "center" bgcolor="#f5d1e7" width="100%">
                <img alt = "app_icon" width = "120" src="logo.png"/>
                <h1><center>Tutor Search </center></h1>
                </table>
        <input style={{
          fontSize: 24,
          display: 'block',
          width: "99%",
          paddingTop: 8,
          paddingBottom: 8,
          paddingLeft: 16
        }} onChange = {this.searchChangeHandler.bind(this)} placeholder = "Enter search "/>
      <table align="center">
        <td>วัน <select name="Var" onChange={this.selectDayChangeHandler.bind(this)} value={this.state.value}>
          <option> เลือกวัน </option>
          <option value="mon">mon</option>
          <option value="tue">tue</option>
          <option value="wed">wed</option>
          <option value="thu">thu</option>
          <option value="fri">fri</option>
          <option value="sat">sat</option>
          <option value="sun">sun</option>
        </select></td>
        <td>เวลา <select name="Var" onChange={this.selectDayChangeHandler.bind(this)} value={this.state.value}>
          <option> ช่วงเวลา </option>
          <option value="เช้า">เช้า</option>
          <option value="เที่ยง">เที่ยง</option>
          <option value="บ่าย">บ่าย</option>
          <option value="เย็น">เย็น</option>
        </select></td>
      </table>
        {this.state.rows}
        
      </div>
    );
  }
}

export default App;
