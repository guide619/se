import React from 'react'
import './CourseRow.css';
class CourseRow extends React.Component{
    render(){
        return<table className = "fixed" key={this.props.course.courseID} >
        <tbody>
            <tr className = "tr" >
                <td><img alt = "app_icon" width = "120" src="student.png"/></td>
               <td align ="left"> {this.props.course.subject}</td>
                    <td>{this.props.course.detail}</td>
                    <td>{this.props.course.day}</td>
                    <td>{this.props.course.time}</td>
                    <td><button type="button">Teach </button>
               </td>
            </tr>
            </tbody>
        </table>
    }

}
export default CourseRow
 